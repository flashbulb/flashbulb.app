# Flashbulb, the Web site

Flashbulb is an Android media app for the fediverse. This is the repository for
the Web site that describes it.

If you agree to the [license] and the [code of conduct], and you wish to
contribute, find all the details about that in [the contributing guide].

## Author

Copyright 2018-2020 Mike Burns. Licensed under [CC BY 4.0].

The [code of conduct] is via [Contributor Covenant], released under the
[Creative Commons Attribution 4.0 International Public License].

[license]: LICENSE
[CC BY 4.0]: https://creativecommons.org/licenses/by/4.0/
[Creative Commons Attribution 4.0 International Public License]: https://github.com/ContributorCovenant/contributor_covenant/blob/master/LICENSE.md
[code of conduct]: doc/CODE_OF_CONDUCT.md
[the contributing guide]: doc/CONTRIBUTING.md
[Contributor Covenant]: https://www.contributor-covenant.org/
