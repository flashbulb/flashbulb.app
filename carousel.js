(function(){
  var box = document.querySelector('.carousel');
  var items = box.querySelectorAll('figure');
  var counter = 0;
  var amount = items.length;
  var current = items[0];

  box.classList.add('carousel-active');

  function navigate(direction) {
    current.classList.remove('carousel-current');
    counter = counter + direction;

    if (direction === -1 && counter < 0) { 
      counter = amount - 1; 
    }

    if (direction === 1 && !items[counter]) { 
      counter = 0;
    }

    current = items[counter];
    current.classList.add('carousel-current');
  }

  for (var i = 0; i < items.length; i++) {
    var figure = items[i];

    var caption = figure.querySelector('figcaption');
    figure.removeChild(caption);

    var div = document.createElement('div');
    div.classList.add('carousel-actions');

    var prevButton = document.createElement('button');
    prevButton.innerText = '◀';

    var nextButton = document.createElement('button');
    nextButton.innerText = '▶';

    div.appendChild(prevButton);
    div.appendChild(caption);
    div.appendChild(nextButton);
    figure.appendChild(div);

    nextButton.addEventListener('click', function(ev) {
      navigate(1);
    });

    prevButton.addEventListener('click', function(ev) {
      navigate(-1);
    });
  }

  navigate(0);
})();
